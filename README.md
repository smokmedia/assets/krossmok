# Krossmok

A variation of [kross-hugo](https://github.com/themefisher/kross-hugo) theme, by Smok Media.

## Example Site

For running the exampleSite:

```
cd exampleSite
hugo --themesDir=../..  --theme=krossmok serve
```

## License

MIT 